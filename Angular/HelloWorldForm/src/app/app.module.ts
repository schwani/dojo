import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HalloWorldInputComponent } from './hallo-world-input/hallo-world-input.component';

@NgModule({
  declarations: [
    AppComponent,
    HalloWorldInputComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
