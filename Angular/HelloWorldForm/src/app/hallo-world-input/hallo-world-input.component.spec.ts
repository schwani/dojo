import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HalloWorldInputComponent } from './hallo-world-input.component';

describe('HalloWorldInputComponent', () => {
  let component: HalloWorldInputComponent;
  let fixture: ComponentFixture<HalloWorldInputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HalloWorldInputComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HalloWorldInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
